package INF101.lab2;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.ArrayList;

class Fridge implements IFridge {

    ArrayList<FridgeItem> fridgeItems;
    ArrayList<FridgeItem> expiredItemsFromFridge;
    int maxSize = 20;

    public Fridge() { // Constructor
        fridgeItems = new ArrayList<FridgeItem>();
        expiredItemsFromFridge = new ArrayList<FridgeItem>();
    }

    @Override
    public int nItemsInFridge() {
        return fridgeItems.size();
    }

    @Override
    public int totalSize() {
        return maxSize;
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        if (this.nItemsInFridge() != maxSize) {
            fridgeItems.add(item);
            return true;
        }
        return false;
    }

    @Override
    public void takeOut(FridgeItem item) {
        if (fridgeItems.contains(item)) {
            fridgeItems.remove(item);
        }
        else {
            throw new NoSuchElementException("That item does not exist.");
        }
    }
        
    @Override
    public void emptyFridge() {
        fridgeItems.clear();
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        // Every item in fridgeItems is of type FridgeItem. 
        // FridgeItem is a class with methods we can use to compare the expiration date. 
        for (FridgeItem i : fridgeItems) {
            if (i.hasExpired()) {
                expiredItemsFromFridge.add(i);
            }
        }
        
        for (FridgeItem expiredItem : expiredItemsFromFridge) {
            this.takeOut(expiredItem);
        }
        return expiredItemsFromFridge;
    }
}